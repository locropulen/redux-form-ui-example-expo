// @flow

import React, { Component } from "react";

import { Field, reduxForm } from "redux-form";
import {
	View,
	KeyboardAvoidingView,
	StyleSheet,
	Text,
	StatusBar,
} from "react-native";
import FlatTouchable from "./Components/FlatTouchable";

import { TextField, runner, rules } from "redux-form-ui";
import InputTextField from "./Components/InputTextField";

import { toPhone } from "./normalize";
const NameTextField = TextField(InputTextField);
const PhoneTextField = TextField(InputTextField);
class MyForm extends Component<*> {
	componentDidMount() {
		this.ref // the Field
			.getRenderedComponent() // on Field, returns ReduxFormMaterialUITextField
			.getRenderedComponent()
			.getRenderedComponent() // onTextField
			.focus(); // on Input
	}
	ref: any;
	saveRef = ref => (this.ref = ref);

	render() {
		const { handleSubmit, submitting } = this.props;
		return (
			<View style={styles.container}>
				<StatusBar barStyle="light-content" />
				<View style={styles.header}>
					<Text style={styles.description}>
						This demo shows how to instance React Native Components with
						redux-form cleanly.
					</Text>
				</View>
				<FlatTouchable onPress={handleSubmit} disabled={submitting}>
					Submit
				</FlatTouchable>
				<Field
					name="name"
					component={NameTextField}
					hintText="Name"
					ref={this.saveRef}
					withRef
					floatingLabelText="Name"
					placeholder="Name"
				/>
				<Field
					name="phone"
					component={PhoneTextField}
					selectTextOnFocus={true}
					normalize={toPhone}
					hintText="Phone"
					keyboardType="phone-pad"
					floatingLabelText="Phone"
				/>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#ecf0f1",
	},
	header: {
		paddingTop: 40,
		padding: 20,
		backgroundColor: "#336699",
	},
	description: {
		fontSize: 14,
		color: "white",
	},
	form: {
		alignItems: "flex-start",
	},
});

export default reduxForm({
	form: "example",
	initialValues: {
		name: "Jane Doe",
	},
	validate: values =>
		runner.run(values, [runner.ruleRunner("name", "Name", rules.required)]),
})(MyForm);
