import React from "react";
import { Alert, StyleSheet, Text, View } from "react-native";
// Logger with default options
import { createLogger } from "redux-logger";

import { ThemeProvider } from "redux-form-ui";
import MyForm from "./MyForm";
import { applyMiddleware, createStore, combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";
import { Provider } from "react-redux";

import DefaultTheme from "./DefaultTheme";

const reducer = combineReducers({
	form: reduxFormReducer, // mounted under "form"
});

const logger = createLogger({
	// ...options
});

const store = createStore(reducer, applyMiddleware(logger));

export default class App extends React.Component {
	onSubmit = event => {
		Alert.alert("Title", JSON.stringify(event));
	};
	render() {
		return (
			<ThemeProvider Theme={DefaultTheme}>
				<Provider store={store}>
					<View style={styles.container}>
						<MyForm onSubmit={this.onSubmit} />
					</View>
				</Provider>
			</ThemeProvider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
	},
});
