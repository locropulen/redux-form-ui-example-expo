// @flow

import React from "react";
import { Text, TouchableOpacity } from "react-native";

import { withTheme } from "redux-form-ui";
const FlatTouchable = (props: {
	children: string,
	styleContainer?: any,
	style?: any,
	FlatTouchable: Function,
	theme: any,
}) => {
	const { children, theme, styleContainer, style, ...rest } = props;
	return (
		<TouchableOpacity
			style={[styleContainer, theme.Touchable.Opacity]}
			{...rest}
		>
			<Text style={[style, theme.Touchable.Text]}>{children}</Text>
		</TouchableOpacity>
	);
};
export default withTheme(FlatTouchable);
