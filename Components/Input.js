// @flow

import React from "react";
import { TextInput, StyleSheet } from "react-native";

type Props = {
	style?: Object,
	theme: any,
	onBlur: Function,
	onFocus: Function,
	value: any,
	onChange: Function,
};

class Input extends React.Component<Props> {
	focus() {
		this.input.focus();
	}
	input: any;

	render() {
		const { theme, onChange, onBlur, onFocus, value, ...rest } = this.props; // eslint-disable-line no-unused-vars

		return (
			<TextInput
				style={[theme.TextInput.Input]}
				underlineColorAndroid="transparent"
				onChangeText={onChange}
				ref={input => (this.input = input)}
				editable
				onBlur={onBlur}
				onFocus={onFocus}
				value={value}
				{...rest}
			/>
		);
	}
}

TextInput.defaultProps = {
	style: {},
	theme: {
		TextInput: {},
	},
};

const styles = StyleSheet.create({
	input: {},
});

export default Input;
