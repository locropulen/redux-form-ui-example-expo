// @flow

import React from "react";
import { Text, View } from "react-native";

type Props = {
	children?: string,
	theme: any,
};

const ErrorMessage = (props: Props) => (
	<View style={[props.theme.Error.Container]}>
		<Text style={[props.theme.Error.Message]}>{props.children}</Text>
	</View>
);

ErrorMessage.defaultProps = { style: {} };
ErrorMessage.displayName = "ErrorMessage";

export default ErrorMessage;
