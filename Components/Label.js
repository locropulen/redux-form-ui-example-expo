// @flow

import React from "react";
import { View, StyleSheet, Text } from "react-native";

// console.log(reduxFormUI)
type Props = {
	children?: string,
	theme: any,
};

const Label = (props: Props) => {
	const styles = StyleSheet.create({
		label: props.theme.Label.Container,
		labelText: props.theme.Label.Text,
	});

	return (
		<View style={styles.label}>
			<Text style={styles.labelText}>{props.children}</Text>
		</View>
	);
};

export default Label;
