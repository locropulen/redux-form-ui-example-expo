// @flow

import React from "react";
import Input from "./Input";
import Label from "./Label";
import ErrorMessage from "./ErrorMessage";
import { View, StyleSheet } from "react-native";

type Props = {
	style?: Object,
	theme: any,
	errorText?: string,
	floatingLabelText?: string,
};

class InputTextField extends React.Component<Props> {
	getRenderedComponent() {
		return this.input;
	}
	input: any;

	render() {
		const { style, theme, errorText, floatingLabelText, ...rest } = this.props; // eslint-disable-line no-unused-vars

		return (
			<View style={[theme.TextInput.Container]}>
				{!!floatingLabelText && (
					<Label theme={theme}>{floatingLabelText}</Label>
				)}
				<Input ref={input => (this.input = input)} theme={theme} {...rest} />
				{!!errorText && <ErrorMessage theme={theme}>{errorText}</ErrorMessage>}
			</View>
		);
	}
}
InputTextField.defaultProps = {
	style: {},
};

const styles = StyleSheet.create({
	input: {},
});

export default InputTextField;
