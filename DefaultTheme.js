import * as Colors from "./MaterialColors";

const DefaultTheme = {
	styles: {},
	Error: {
		Container: {
			backgroundColor: Colors.red500,
		},
		Message: {
			color: "white",
		},
	},
	TextInput: {
		Container: {},
		Input: {
			height: 40,
			paddingHorizontal: 5,
			borderWidth: 1,
			borderColor: Colors.grey800,
		},
		Legend: {},
	},
	Label: {
		Container: {
			paddingVertical: 5,
		},
		Text: {},
	},
	Label: {
		Container: {
			paddingVertical: 10,
			backgroundColor: "purple",
		},
		Text: {
			color: "white",
		},
	},
	Colors,
	Touchable: {
		Text: {
			color: "white",
			fontSize: 20,
		},
		Opacity: {
			backgroundColor: "blue",
			paddingVertical: 10,
			justifyContent: "center",
			alignItems: "center",
		},
	},
};

export default DefaultTheme;
